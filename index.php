<?php

include_once "vendor/autoload.php";

use Pondit\Vehicle\LandVehicle\Car;
use Pondit\Vehicle\LandVehicle\Truck;
use Pondit\Vehicle\WaterVehicle\Ship;
use Pondit\Vehicle\AirVehicle\Plane;


$car1 = new Car();
var_dump($car1);

$truck1 = new Truck();
var_dump($truck1);

$ship1 = new Ship();
var_dump($ship1);

$plane = new Plane();
var_dump($plane);
